from whoosh.index import open_dir
from whoosh.qparser import QueryParser, FuzzyTermPlugin
from whoosh import scoring

ix = open_dir("IndexV3")

# searcher_Freq = ix.searcher(weighting = scoring.Frequency())
# searcher_TF_IDF = ix.searcher(weighting = scoring.TF_IDF())
searcher_BM25 = ix.searcher(weighting=scoring.BM25F())

def search_PageRankSorted(term):
    parser = QueryParser("content", ix.schema) 
    parser.add_plugin(FuzzyTermPlugin())
    query = parser.parse(term)
    results = searcher_BM25.search(query)
    toRet = list()
    
    for el in sorted(results, key = lambda result: result["rank"] * result.score, reverse = True): 
        toRet.append(el)
    
    return toRet

# def search_TF_IDF(term):
#     parser = QueryParser("content", ix.schema) 
#     parser.add_plugin(FuzzyTermPlugin())
#     query = parser.parse(term)
#     results = searcher_TF_IDF.search(query)
#     return list(results)

def search_BM25(term):
    parser = QueryParser("content", ix.schema) 
    parser.add_plugin(FuzzyTermPlugin())
    query = parser.parse(term)
    results = searcher_BM25.search(query)
    return list(results)
    
# def search_combined(term):
#     parser = QueryParser("content", ix.schema) 
#     parser.add_plugin(FuzzyTermPlugin())
#     query = parser.parse(term)
#     results = searcher_BM25.search(query)
#     toRet = list()
#     for el in sorted(results, key = lambda result : \
#                      (2 * result["rank"] * result.score) / (result["rank"] + result.score), reverse = True):
#         toRet.append(el)
#     return toRet
        
# def search_percentage(term, pr, sim):
#     assert pr + sim == 1
#     parser = QueryParser("content", ix.schema) 
#     parser.add_plugin(FuzzyTermPlugin())
#     query = parser.parse(term)
#     results = searcher_Freq.search(query)
#     for el in sorted(results, key = lambda result : ((result.score * sim) / result.score + (result["rank"] * pr) / result["rank"])):
#         print(el, el.score)