#!/usr/bin/env python
# -*- coding: utf-8 -*-

from whoosh.index import create_in
from whoosh.fields import *
import os, os.path
from whoosh.analysis import StemmingAnalyzer
from PageRank import adjacency_pagerank
import re
import shutil

def getComputableGraph(path):
    
    pages = dict()
    count = 0
    S = dict()
    rS = dict()
    
    for root, subdirs, files in os.walk(path):
        for el in files:
            if el.startswith("w"):
                with open(str(root + os.sep + el), "r", encoding="utf-8") as f:
                    print("Collecting info from ->", str(root + os.sep + el), count)
                    for line in f:
                        if line.startswith("<doc "):
                            pages[line.split("\"")[5]] =  count
                            S[count] = []
                            rS[count] = []
                            count += 1

    
    count = 0
    for root, subdirs, files in os.walk(path):
        for el in files:
            if el.startswith("w"):
                with open(str(root + os.sep + el), "r", encoding="utf-8") as f:
                    print("Getting links from ->", str(root + os.sep + el), count)
                    count += 1
                    for line in f:
                        if line.startswith("<doc "):
                            source = pages.get(line.split("\"")[5], None)
                        else:
                            for link in re.findall("<a .*?</a>",line):
                                try:
                                    dest = pages.get(link.split("\"")[1].replace('%20', " "), None)
                                    if dest != None and source != None:
                                        S[source].append(dest)
                                        rS[dest].append(source)
                                except:
                                    pass
    return  adjacency_pagerank(S, rS), pages

class Document():
    def __init__(self,t,u,c):
        self.title = t
        self.url = u
        self.content = c
        self.rank = 0
        self.links = list()

    def setRank(self,r):
        self.rank = r

def main():
    INDEX_PATH = "IndexV3"
    EXTRACTED_FILES_PATH = "ModifiedFiles"
    
    if not os.path.exists(INDEX_PATH):
        os.mkdir(INDEX_PATH)
    
    print("Extracting info from XML dump...")
    
    for file in os.listdir("../TestSet"):
        os.system(str("python3 WikiExtractor.py -o ModifiedFiles/" + file + " ../TestSet/" + file + " -l --processes 4"))
    
    schema = Schema(
        title = TEXT(stored=True),
        url = ID(stored=True),
        content = TEXT(analyzer = StemmingAnalyzer()),
        rank = NUMERIC(numtype=float, stored=True)
        )
    ix = create_in(INDEX_PATH, schema)
    writer = ix.writer(procs=4, multisegment=True)
    
    pr, pages = getComputableGraph(EXTRACTED_FILES_PATH)
    
    for root, subdirs, files in os.walk(EXTRACTED_FILES_PATH):
        for el in files:
            if el.startswith("w"):
                with open(str(root + os.sep + el), "r", encoding="utf-8") as f:
                    print("Parsing ->", str(root + os.sep + el))
                    for line in f:
                        if line.startswith("<doc "):
                            l = line.split("\"")
                            doc = Document(l[5], l[3], "")
                        elif line.startswith("</doc>"):
                            writer.add_document(
                                    title = doc.title,
                                    url = doc.url,
                                    content = doc.content,
                                    rank = float(pr.get(pages.get(doc.title,None),0.0))
                                )
                        else:
                            for link in re.findall("<a .*?</a>",line):
                                try:
                                    line = line.replace(str(link), str(link.split(">")[1].split("<")[0]))
                                except:
                                    pass
                            doc.content += line
    
    print("Committing...")
    writer.commit()
    
if __name__ == "__main__":
    main()
    try:
        shutil.rmtree("ModifiedFiles")
    except OSError as e:
        print("Error: %s : %s" % ("ModifiedFiles", e.strerror))






