from whoosh.index import open_dir
from whoosh.qparser import QueryParser, FuzzyTermPlugin
from whoosh import scoring

ix = open_dir("IndexV0")

searcher_TF_IDF = ix.searcher(weighting = scoring.TF_IDF())

def search_TF_IDF(term):
    parser = QueryParser("content", ix.schema) 
    parser.add_plugin(FuzzyTermPlugin())
    query = parser.parse(term)
    results = searcher_TF_IDF.search(query)
    return list(results)
