from whoosh.index import create_in, open_dir 
from whoosh.fields import *
import os, os.path
from whoosh.analysis import StemmingAnalyzer, FancyAnalyzer, LanguageAnalyzer, NgramAnalyzer
import xml.sax




if not os.path.exists("IndexV1"):
    os.mkdir("IndexV1")


schema = Schema(
    title = TEXT(stored=True),
    url = ID(stored=True),
    # Da provare FancyAnalyzer(), NgramAnalyzer() e LanguageAnalyzer
    content = TEXT(analyzer = LanguageAnalyzer("en"))
    ) 


class MyContentHandler(xml.sax.ContentHandler):
    def __init__(self, writer):
        self.writer = writer

    def startElement(self, name, attr):
        self.state = name 
        if name == "page":
            self.title = ""
            self.text = ""
            print ("---- PAGE ----")

    def endElement(self, name):
        if name == "page":
            self.writer.add_document(title = self.title,
                                    url = "https://en.wikipedia.org/wiki/" + self.title.replace(" ", "_"),
                                    content = self.text)
            print ("----/PAGE ----")
        self.state = ""

    # def endDocument(self):
    #     print("Committing...")
    #     self.writer.commit()
    #     print("Committed")


    def characters(self, ch):
        if self.state == "title":
            self.title = ch.strip()
            print(" ",ch)
        elif self.state == "text":
            self.text += ch
        

parser = xml.sax.make_parser()
ix = create_in("IndexV1", schema)
myWriter = ix.writer(procs=1, multisegment=True)
parser.setContentHandler(MyContentHandler(myWriter))
for file in os.listdir("../TestSet"):
    parser.parse("../TestSet/" + file)

print("----Committing----")
myWriter.commit()
print("Committed")

