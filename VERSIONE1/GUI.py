import tkinter as tk
import searcher
import webbrowser


root = tk.Tk()
root.title("WikiSearch")
root.geometry("960x540")
root.configure(bg='#80c1ff')

def get_results(term):
    res = searcher.search_BM25(term)
    print(len(res))
    for el in res:
        tk.Button(lower_frame, text=f"{el['title']}", \
         command=lambda j=el : open_browser(j)).place(rely=0.1*res.index(el), relheight=0.1, relwidth=1)

def open_browser(j):
    webbrowser.open(j['url'])


frame = tk.Frame(root, bg='#80c1ff', bd=5)
frame.place(relx=0.5, rely=0.05, relwidth=0.75, relheight=0.07, anchor='n')
button = tk.Button(frame, text="Search!", font=40, command=lambda: get_results(entry.get()))
button.place(relx=0.7, relheight=1, relwidth=0.3)
entry = tk.Entry(frame, font=40)
entry.place(relwidth=0.65, relheight=1)

lower_frame = tk.Frame(root, bg='#80c1ff', bd=10)
lower_frame.place(relx=0.5, rely=0.15, relwidth=0.75, relheight=0.77, anchor='n')

label = tk.Label(lower_frame, bg ='#ffffff')
label.place(relwidth=1, relheight=1)

root.mainloop()
