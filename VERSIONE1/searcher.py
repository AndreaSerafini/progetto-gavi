from whoosh.index import open_dir
from whoosh.qparser import QueryParser, FuzzyTermPlugin
from whoosh import scoring

ix = open_dir("IndexV1")

searcher_BM25 = ix.searcher(weighting=scoring.BM25F())

def search_BM25(term):
    parser = QueryParser("content", ix.schema) 
    parser.add_plugin(FuzzyTermPlugin())
    query = parser.parse(term)
    results = searcher_BM25.search(query)
    return list(results)
