import searcher
from math import log2

def load_db(path):
    rank = dict()
    actual = ''

    with open(path, 'r') as file:
        for line in file:
            if line.startswith("https"):
                rank[actual].append(line.split("|")[1].strip()) 
            else:
                actual = line.strip()
                rank[actual] = list()

    return rank

def average_precision(query, rank):
    
    res = searcher.search_BM25(query)
    # for el in res:
    #     print(el['title'])
    rk = rank[query][:10]
    # print(rk)

    seen_results = 0
    seen_rel_results = 0
    precision_list = list()

    for el in res:
        seen_results += 1
        if el['title'] in rk:
            seen_rel_results += 1
            precision_list.append(seen_rel_results/seen_results)


    return sum(precision_list) / 10

def mean_average_precision(rank):
    queries = ["Apple",
    "DNA",
    "Roman Empire",
    "Solar energy",
    'Epigenetics',
    'Statistical Significance',
    'Hollywood',
    'Steve Jobs',
    'Maya',
    'The Maya',
    'Microsoft',
    'Precision',
    'Tuscany',
    '99 balloons',
    'Computer Programming',
    'Financial meltdown',
    'Justin Timberlake',
    'Least Squares',
    'Mars robots',
    'Page six',
    'Triple Cross',
    'US Constitution',
    'Eye of Horus',
    'Madam I’m Adam',
    'Mean Average Precision',
    'Physics Nobel Prizes',
    'Read the manual',
    'Spanish Civil War',
    'Do geese see god',
    'Much ado about nothing']

    precision_list = list()
    for query in queries:
        ap = average_precision(query, rank)
        print(query,';', ap)
        precision_list.append(ap)

    return sum(precision_list) / len(precision_list)


def ndcg(query, rank):
    rk = rank[query]

    optimal = dict()
    for el in rk:
        optimal[el] = get_score(rk.index(el))

    res = searcher.search_BM25(query)
    ndcg = 0
    for i in range(len(res)):
        if i < 2:
            ndcg += optimal.get(res[i]['title'], 0)
        else:
            ndcg += optimal.get(res[i]['title'], 0) / log2(i+1)
            
    return ndcg / 17.5779603
    
def avg_ndcg(rank):
    queries = ["Apple",
    "DNA",
    "Roman Empire",
    "Solar energy",
    'Epigenetics',
    'Statistical Significance',
    'Hollywood',
    'Steve Jobs',
    'Maya',
    'The Maya',
    'Microsoft',
    'Precision',
    'Tuscany',
    '99 balloons',
    'Computer Programming',
    'Financial meltdown',
    'Justin Timberlake',
    'Least Squares',
    'Mars robots',
    'Page six',
    'Triple Cross',
    'US Constitution',
    'Eye of Horus',
    'Madam I’m Adam',
    'Mean Average Precision',
    'Physics Nobel Prizes',
    'Read the manual',
    'Spanish Civil War',
    'Do geese see god',
    'Much ado about nothing']

    ndcg_list = list()
    for query in queries:
        ap = ndcg(query, rank)
        print(query,';', ap)
        ndcg_list.append(ap)

    return sum(ndcg_list) / len(ndcg_list)


def get_score(pos):
    d = dict()
    d[1] = 6
    d[2] = 5
    d[3] = 4
    d[4] = 3
    d[5] = 2
    d[6] = 1
    d[7] = 1
    d[8] = 1
    d[9] = 1
    d[10] = 1
    return d.get(pos+1, 0)

if __name__ == "__main__":
    rk = load_db("../classifica.txt")
    print("---- Mean average precision ----")
    print(mean_average_precision(rk))
    print("---- AVG NDCG ----")
    print(avg_ndcg(rk))
