from googlesearch import search
import os.path
import time

def getPages(query):
    return search(query, num = 10, pause=0.0)

def downloadFromWiki(link,count_tit):
    import requests
    nome = link.split("/")[-1].replace("_"," ")
    URL = "https://en.wikipedia.org/wiki/Special:Export/" + nome
    response = requests.get(URL)
    print("Ho scaricato", nome)
    with open('TestSet/' + str(count_tit) + '.xml', 'wb') as file:
        file.write(response.content)
    

queries = ["Apple",
 "DNA",
 "Roman Empire",
 "Solar energy",
 'Epigenetics',
 'Statistical Significance',
 'Hollywood',
 'Steve Jobs',
 'Maya',
 'The Maya',
 'Microsoft',
 'Precision',
 'Tuscany',
 '99 balloons',
 'Computer Programming',
 'Financial meltdown',
 'Justin Timberlake',
 'Least Squares',
 'Mars robots',
 'Page six',
 'Triple Cross',
 'US Constitution',
 'Eye of Horus',
 'Madam I’m Adam',
 'Mean Average Precision',
 'Physics Nobel Prizes',
 'Read the manual',
 'Spanish Civil War',
 'Do geese see god',
 'Much ado about nothing']

pages = set()
if not os.path.exists("TestSet"):
        os.mkdir("TestSet")

count_tit = 0
for query in queries:
    with open("TestSet/classifica.txt", "a") as f:
        f.write(query + "\n")
    count = 0
    for page in getPages(query + " site:en.wikipedia.org"):
        if '#' not in page and page.count(":") < 2:
            with open("TestSet/classifica.txt", "a") as f:
                f.write(page+ "|" + page.split("/")[-1].replace("_"," ") + "\n")
            if page not in pages:
                downloadFromWiki(page, count_tit)
                count_tit += 1
                pages.add(page)
            count += 1
            if count == 30:
                break
        time.sleep(10)
